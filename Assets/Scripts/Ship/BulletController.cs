// // Copyright © 2020 Yurij Bein. All rights reserved.
// // Author: Yurij Bein (AkinRavus)
// // License: CC BY-NC-SA 4.0

using UnityEngine;
using YurijBein.StarAdventure.Ship.Modules;

namespace YurijBein.StarAdventure.Ship
{
    [RequireComponent(typeof(Collider), typeof(Rigidbody))]
    public class BulletController : MonoBehaviour
    {
        public Hit hit;
        public float speed = 1000f;

        private void Start()
        {
            gameObject.GetComponent<Rigidbody>().AddForce(Vector3.forward * speed);
            Debug.Log($"Wased fire with damage {hit.baseDamage}");
        }
    }
}