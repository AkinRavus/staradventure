// // Copyright © 2020 Yurij Bein. All rights reserved.
// // Author: Yurij Bein (AkinRavus)
// // License: CC BY-NC-SA 4.0

namespace YurijBein.StarAdventure.Ship
{
    public interface ITick
    {
        void Tick();
    }
}