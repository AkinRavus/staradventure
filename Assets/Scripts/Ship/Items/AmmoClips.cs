// // Copyright © 2020 Yurij Bein. All rights reserved.
// // Author: Yurij Bein (AkinRavus)
// // License: CC BY-NC-SA 4.0

using UnityEngine;

namespace YurijBein.StarAdventure.Ship.Items
{
    [CreateAssetMenu(fileName = "Ammo", menuName = "Space/Ammo", order = 0)]
    public class AmmoClips : Item
    {
        public AmmoType type;
        public int maxAmount;
        private int amount;
        public int bonusToDamage;
        public int bonusToMinDamage;
        public int bonusAgainstArmor;
        public int bonusAgainstShield;

        public int GetNumberOfShots(int count)
        {
            amount -= count;
            return amount < 0 ? (count+amount) : count;
        }

        public bool IsEmpty()
        {
            return amount <= 0;
        }
        public int Amount
        {
            get => amount;
            set => amount = Mathf.Clamp(value, 0, maxAmount);
        }
    }

    public enum AmmoType : byte
    {
        Bullet,
        Laser
    }
}