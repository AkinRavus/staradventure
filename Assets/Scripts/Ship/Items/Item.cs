// // Copyright © 2020 Yurij Bein. All rights reserved.
// // Author: Yurij Bein (AkinRavus)
// // License: CC BY-NC-SA 4.0

using UnityEngine;

namespace YurijBein.StarAdventure.Ship.Items
{
    [CreateAssetMenu(fileName = "Custom item", menuName = "Space/Item", order = 0)]
    public class Item : ScriptableObject
    {
        [Header("Item description")] [Space]
        public string itemName;
        public string description;
        public double cost;
        public float weight;
        public bool isQuestItem;
        public bool isFitsPersonInventory;
    }
}