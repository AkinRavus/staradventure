// // Copyright © 2020 Yurij Bein. All rights reserved.
// // Author: Yurij Bein (AkinRavus)
// // License: CC BY-NC-SA 4.0

using UnityEngine;

namespace YurijBein.StarAdventure.Ship.Modules
{
    [CreateAssetMenu(fileName = "Engine", menuName = "Ship/Engine", order = 0)]
    public class Engine : Module
    {
        public float rotationSpeed;
        public float shipRotationSpeed;
        public float forwardSpeed;
        public float backwardSpeed;
        public float horizontalSpeed;
        public float verticalSpeed;
        public override void LevelUp()
        {
            throw new System.NotImplementedException();
        }

        public override void SetUp(Ship ship)
        {
            throw new System.NotImplementedException();
        }
    }
}