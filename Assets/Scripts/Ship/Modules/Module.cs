// // Copyright © 2020 Yurij Bein. All rights reserved.
// // Author: Yurij Bein (AkinRavus)
// // License: CC BY-NC-SA 4.0

using System;
using UnityEngine;
using YurijBein.StarAdventure.Ship.Items;

namespace YurijBein.StarAdventure.Ship.Modules
{
    public abstract class Module : Item
    {
        [Header("Module base")] [Space]
        public byte maxLevel;
        public byte currentLevel;
        public double[] costToUpgrade;
        public int maxDurability;
        [SerializeField] protected int currentDurability;
        [SerializeField] protected int energyConsumption;
        public bool isOnline;

        public int EnergyConsumption
        {
            get => energyConsumption;
            set => energyConsumption = value;
        }

        public int CurrentDurability
        {
            get => currentDurability;
            set
            {
                currentDurability = Mathf.Clamp(value, 0, maxDurability);
                if (currentDurability == 0)
                    isOnline = false;
            }
        }
        public abstract void LevelUp();
        public abstract void SetUp(Ship ship);

        public virtual void TakeHit(Hit hit)
        {
            currentDurability -= hit.baseDamage;
            CheckDurability();
        }
        public void CheckDurability()
        {
            isOnline = CurrentDurability > 0;
        }
    }
}