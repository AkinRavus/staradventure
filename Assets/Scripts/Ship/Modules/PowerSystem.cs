// // Copyright © 2020 Yurij Bein. All rights reserved.
// // Author: Yurij Bein (AkinRavus)
// // License: CC BY-NC-SA 4.0

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace YurijBein.StarAdventure.Ship.Modules
{
    [CreateAssetMenu(fileName = "Power System", menuName = "Ship/Power System", order = 0)]
    public class PowerSystem : Module, ITick
    {
        [Header("Power System")] [Space]
        public int maxCapacity;
        public int capacity;
        public int totalConsumed;
        public int baseGenerated;
        public int forceModeBonus;
        public byte safeForce;
        public byte maxSafeForce;
        public int safeTick;
        public byte damageToDurability;
        
        [HideInInspector] public int safeTickLeft;
        private Ship _ship;
        private byte _forceMode;
        private bool _isSafeForce;
        
        public void CalculatePowerConsuming(List<Module> activeModules)
        {
            foreach (var module in activeModules)
                totalConsumed += module.EnergyConsumption;
        }
        
        public void Tick()
        {
            if (_forceMode > 0 && !_isSafeForce)
                CurrentDurability -= damageToDurability * _forceMode;
            if (_isSafeForce)
            {
                if (safeTickLeft == 0) ForceModeOff();
                else
                    safeTickLeft--;
            }
            
            capacity -= totalConsumed - baseGenerated - forceModeBonus * _forceMode;
            _ship.isOnline = capacity >= 0 && isOnline;
        }
        public void ForceModeUp()
        {
            if (safeForce > 0)
            {
                _isSafeForce = true;
                safeForce--;
                safeTickLeft = safeTick;
            }
            _forceMode++;
        }
        public void ForceModeOff()
        {
            _isSafeForce = false;
            _forceMode = 0;
        }
        
        public override void LevelUp()
        {
            throw new NotImplementedException();
        }

        public override void SetUp(Ship ship)
        {
            _ship = ship;
            capacity = maxCapacity;
            ForceModeOff();
            CalculatePowerConsuming(ship.GetModules());
        }
    }
}