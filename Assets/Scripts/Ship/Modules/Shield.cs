// // Copyright © 2020 Yurij Bein. All rights reserved.
// // Author: Yurij Bein (AkinRavus)
// // License: CC BY-NC-SA 4.0

using System;
using UnityEngine;

namespace YurijBein.StarAdventure.Ship.Modules
{
    [CreateAssetMenu(fileName = "Shield", menuName = "Ship/Shield", order = 0)]
    public class Shield : Module, ITick
    {
        [Header("Shield")] [Space] 
        public int capacity;
        public int maxCapacity;
        public int damageResist;
        public float timeToReloaded;
        public float leftTimeToReloaded;

        private Ship _ship;
        private bool _reloadedInProgress;
        public override void LevelUp()
        {
            throw new NotImplementedException();
        }

        public override void SetUp(Ship ship)
        {
            _ship = ship;
            capacity = maxCapacity;
        }

        public override void TakeHit(Hit hit)
        {
            capacity -= hit.DamageToShield() - damageResist;
            if (capacity < 0)
            {
                var leftDamage = capacity;
                hit.baseDamage = leftDamage;
                CurrentDurability -= leftDamage;
                capacity = 0;
                _ship.hull.TakeHit(hit);
            }
            CheckDurability();
            CheckCapacity();
        }

        public void CheckCapacity()
        {
            if (capacity < maxCapacity)
                StartReloaded();
        }

        public void StartReloaded()
        {
            leftTimeToReloaded = timeToReloaded;
            _reloadedInProgress = true;
        }

        public void Tick()
        {
            if (_reloadedInProgress)
            {
                if (leftTimeToReloaded > 0)
                    leftTimeToReloaded -= Time.deltaTime;
                else
                {
                    FinishReloaded();
                }
            }
        }

        public void FinishReloaded()
        {
            _reloadedInProgress = false;
            capacity = maxCapacity;
        }
    }
}