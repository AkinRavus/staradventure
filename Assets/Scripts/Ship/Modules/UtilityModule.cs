// // Copyright © 2020 Yurij Bein. All rights reserved.
// // Author: Yurij Bein (AkinRavus)
// // License: CC BY-NC-SA 4.0

using System;
using UnityEngine;

namespace YurijBein.StarAdventure.Ship.Modules
{
    [CreateAssetMenu(fileName = "Utility Module", menuName = "Ship/Utility module", order = 0)]
    public class UtilityModule : Module
    {
        public override void LevelUp()
        {
            throw new NotImplementedException();
        }

        public override void SetUp(Ship ship)
        {
            throw new NotImplementedException();
        }
    }
}