// // Copyright © 2020 Yurij Bein. All rights reserved.
// // Author: Yurij Bein (AkinRavus)
// // License: CC BY-NC-SA 4.0

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using YurijBein.StarAdventure.Ship.Items;

namespace YurijBein.StarAdventure.Ship.Modules
{
    [CreateAssetMenu(fileName = "Weapon", menuName = "Ship/Weapon", order = 0)]
    public class Weapon : Module
    {
        [Header("Weapon")] [Space]
        public WeaponType type;
        public int damage;
        public int minDamage;
        public int armorPiercing;
        public int shieldPiercing;
        public AmmoType ammoType;
        private List<AmmoClips> _ammoClips;
        public byte maxNumberOfClips;
        public byte ammoPerShot;
        public override void LevelUp()
        {
            throw new NotImplementedException();
        }

        public override void SetUp(Ship ship)
        {
            ReloadIfEmpty();
        }

        public List<Hit> Fire()
        {
            var hits = new List<Hit>();
            if (_ammoClips.Any())
                for (int shotsCount = _ammoClips[0].GetNumberOfShots(ammoPerShot); shotsCount > 0; shotsCount--)
                    hits.Add(CreateHit());
            ReloadIfEmpty();
            return hits;
        }

        public void ReloadIfEmpty()
        {
            if (_ammoClips.Any())
                if (_ammoClips[0].IsEmpty())
                    Reload();
        }

        public bool Reload()
        {
            if (_ammoClips.Any())
            {
                _ammoClips.RemoveAt(0);
                return true;
            }
            else return false;
        }
        private Hit CreateHit()
        {
            var hit = new Hit
            {
                baseDamage = damage + _ammoClips[0].bonusToDamage,
                minDamage = minDamage + _ammoClips[0].bonusToMinDamage,
                armorPiercing = armorPiercing + _ammoClips[0].bonusAgainstArmor,
                shieldPiercing = shieldPiercing + _ammoClips[0].bonusAgainstShield
            };
            
            return hit;
        }
        public List<AmmoClips> GetAmmoClipsList()
        {
            return _ammoClips;
        }

        public void AddAmmoClips(AmmoClips ammoClips)
        {
            if(ammoClips.type == ammoType)
                if(_ammoClips.Count < maxNumberOfClips)
                    _ammoClips.Add(ammoClips);
        }
    }

    public enum WeaponType
    {
        Main,
        Secondary,
        Turret
    }
    public struct Hit
    {
        public int baseDamage;
        public int minDamage;
        public int armorPiercing;
        public int shieldPiercing;

        public Hit(int baseDamage, int minDamage, int armorPiercing, int shieldPiercing)
        {
            this.baseDamage = baseDamage;
            this.minDamage = minDamage;
            this.armorPiercing = armorPiercing;
            this.shieldPiercing = shieldPiercing;
        }

        public int DamageToShield()
        {
            return baseDamage + shieldPiercing;
        }
        public int DamageToArmor()
        {
            return baseDamage + armorPiercing;
        }
    }
}