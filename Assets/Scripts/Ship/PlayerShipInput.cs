// // Copyright © 2020 Yurij Bein. All rights reserved.
// // Author: Yurij Bein (AkinRavus)
// // License: CC BY-NC-SA 4.0

using UnityEngine;
using YurijBein.StarAdventure.Ship.Modules;

namespace YurijBein.StarAdventure.Ship
{
    [RequireComponent(typeof(ShipController))]
    public class PlayerShipInput : MonoBehaviour
    {
        private ShipController _shipController;
        private float _halfOfHeightScreen;
        private float _halfOfWidthScreen;
        private float _deadZone = 0.3f;
        
        private void Awake()
        {
            _shipController = gameObject.GetComponent<ShipController>();
            _halfOfHeightScreen = Screen.height / 2f;
            _halfOfWidthScreen = Screen.width / 2f;
        }

        private void Update()
        {
            var mousePosition = Input.mousePosition;
            var horizontalRotation = (_halfOfWidthScreen - mousePosition.x)/-_halfOfWidthScreen;
            var verticalRotation = (_halfOfHeightScreen - mousePosition.y)/_halfOfHeightScreen;

            if (Mathf.Abs(verticalRotation) <= _deadZone)
                verticalRotation = 0;
            if (Mathf.Abs(horizontalRotation) <= _deadZone)
                horizontalRotation = 0;
            
            _shipController.HorizontalRotation = horizontalRotation;
            _shipController.VerticalRotation = verticalRotation;
            
            _shipController.ForwardAcceleration = Input.GetAxis("Vertical");
            _shipController.HorizontalAcceleration = Input.GetAxis("Horizontal");
            _shipController.VerticalAcceleration = Input.GetAxis("Up&Down");
            _shipController.ShipRotation = Input.GetAxis("ShipRotation");

            if (Input.GetMouseButtonDown(0))
                _shipController.Fire(WeaponType.Main);
            if (Input.GetMouseButtonDown(1))
                _shipController.Fire(WeaponType.Secondary);

            if(Input.GetKeyDown(KeyCode.LeftShift))
                _shipController.controlledShip.powerSystem.ForceModeUp();
            if(Input.GetKeyDown(KeyCode.F))
                _shipController.controlledShip.powerSystem.ForceModeOff();
        }
    }
}