// // Copyright © 2020 Yurij Bein. All rights reserved.
// // Author: Yurij Bein (AkinRavus)
// // License: CC BY-NC-SA 4.0

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using YurijBein.StarAdventure.Ship.Modules;

namespace YurijBein.StarAdventure.Ship
{
    [CreateAssetMenu(fileName = "Ship", menuName = "Space/Ship", order = 0)]
    public class Ship : ScriptableObject, ITick
    {
        public Engine engine;
        public Hull hull;
        public PowerSystem powerSystem;
        public Shield shield;
        public ResidentialModule residentialModule;
        public Cargo cargo;
        public byte weaponSlots;
        public byte turretSlots;
        public byte utilitySlots;
        public List<Weapon> weapons;
        public List<Weapon> turrets;
        public List<UtilityModule> utilityModules;
        public bool isOnline;
        
        public void Tick()
        {
            powerSystem.Tick();
            shield.Tick();
        }
        public List<Hit> Fire(WeaponType weaponType)
        {
            if (weaponType == WeaponType.Turret) throw new ArgumentException("Fire method only accept not automate weapons");
            return weapons.First(weapon => weapon.type == weaponType).Fire();
        }

        public void SetUp()
        {
            foreach (var module in GetModules())
                module.SetUp(this);
        }

        public List<Module> GetModules()
        {
            var modules = new List<Module>()
            {
                engine,
                hull,
                powerSystem,
                shield,
                residentialModule,
                cargo
            };
            modules.AddRange(weapons);
            modules.AddRange(turrets);
            modules.AddRange(utilityModules);
            return modules;
        }
    }
}