// // Copyright © 2020 Yurij Bein. All rights reserved.
// // Author: Yurij Bein (AkinRavus)
// // License: CC BY-NC-SA 4.0

using UnityEngine;
using YurijBein.StarAdventure.Ship.Modules;

namespace YurijBein.StarAdventure.Ship
{
    [RequireComponent(typeof(Rigidbody))]
    public class ShipController : MonoBehaviour
    {
        public Ship controlledShip;
        [Header("Move input")]
        [Range(-1, 1)] [SerializeField] 
        private float forwardAcceleration;
        [Range(-1, 1)] [SerializeField]
        private float horizontalAcceleration;
        [Range(-1, 1)] [SerializeField]
        private float verticalAcceleration;
        [Range(-1, 1)] [SerializeField]
        private float horizontalRotation;
        [Range(-1, 1)] [SerializeField]
        private float verticalRotation;
        [Range(-1, 1)] [SerializeField] 
        private float shipRotation;

        [Header("Shoot")]
        public GameObject bulletPrefab;
        public Transform createPosition;
        private BulletController _bulletController;

        private Rigidbody _rigidbody;

        private void Awake()
        {
            controlledShip.SetUp();
            _rigidbody = gameObject.GetComponent<Rigidbody>();
            _rigidbody.drag = controlledShip.hull.drag;
            _rigidbody.angularDrag = controlledShip.hull.drag;

            _bulletController = bulletPrefab.GetComponent<BulletController>();
        }

        private void FixedUpdate()
        {
            Move();
            controlledShip.Tick();
        }

        public void Fire(WeaponType weaponType)
        {
            foreach (var hit in controlledShip.Fire(weaponType))
            {
                _bulletController.hit = hit;
                Instantiate(bulletPrefab, createPosition);
            }
        }

        private void Move()
        {
            var relativeForce = new Vector3(horizontalAcceleration, verticalAcceleration, forwardAcceleration);
            relativeForce.x *= controlledShip.engine.horizontalSpeed;
            relativeForce.y *= controlledShip.engine.verticalSpeed;
            relativeForce.z *= forwardAcceleration > 0 ? controlledShip.engine.forwardSpeed : controlledShip.engine.backwardSpeed;

            var relativeTorque = new Vector3(verticalRotation, horizontalRotation, shipRotation);
            relativeTorque.x *= controlledShip.engine.rotationSpeed;
            relativeTorque.y *= controlledShip.engine.rotationSpeed;
            relativeTorque.z *= controlledShip.engine.shipRotationSpeed;
            
            _rigidbody.AddRelativeForce(relativeForce, ForceMode.Force);
            _rigidbody.AddRelativeTorque(relativeTorque, ForceMode.Force);
        }
        public float ForwardAcceleration
        {
            get => forwardAcceleration;
            set => forwardAcceleration = Mathf.Clamp(value, -1, 1);
        }

        public float HorizontalAcceleration
        {
            get => horizontalAcceleration;
            set => horizontalAcceleration = Mathf.Clamp(value, -1, 1);
        }

        public float VerticalAcceleration
        {
            get => verticalAcceleration;
            set => verticalAcceleration = Mathf.Clamp(value, -1, 1);
        }

        public float HorizontalRotation
        {
            get => horizontalRotation;
            set => horizontalRotation = Mathf.Clamp(value, -1, 1);
        }

        public float VerticalRotation
        {
            get => verticalRotation;
            set => verticalRotation = Mathf.Clamp(value, -1, 1);
        }
        public float ShipRotation
        {
            get => shipRotation;
            set => shipRotation = Mathf.Clamp(value, -1, 1);
        }
    }
}